#!/bin/bash

ID=$(id -u) 
#COMMENT a variable which will store uid also id-u will give you outpur for uid of user 

echo -e "\e[31m Chekcing ID\e[0m"
if [ $ID -ne 0 ] ; then
    echo -e "\e[31m You need to execure this script as a root user or with a sudo command \e[0m"

exit 2
fi 

echo -n -e  "\e[31m Installing Web Server\e[0m"
sudo yum install httpd -y &> /tmp/stack.log

if [ $? -eq 0 ] ; then
    echo "sucess"
else 
    echo "Fail"
fi 